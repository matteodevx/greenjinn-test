import { DynamoDB } from "aws-sdk";
import { SECRETS_TABLE_NAME, secretKey } from "./utils";

const EMAIL_API_KEY = process.env.EMAIL_API_KEY;

if (EMAIL_API_KEY == null) {
  throw new Error("Missing EMAIL_API_KEY");
}

async function createTableIfNotExists() {
  const db = new DynamoDB();
  const exists = await db
    .describeTable({ TableName: SECRETS_TABLE_NAME })
    .promise()
    .then(() => true)
    .catch((err) => {
      if (err.code === "ResourceNotFoundException") {
        return false;
      }
      throw err;
    });
  if (!exists) {
    await db
      .createTable({
        TableName: SECRETS_TABLE_NAME,
        AttributeDefinitions: [
          {
            AttributeName: "key",
            AttributeType: "S",
          },
        ],
        KeySchema: [
          {
            AttributeName: "key",
            KeyType: "HASH",
          },
        ],
        ProvisionedThroughput: {
          ReadCapacityUnits: 1,
          WriteCapacityUnits: 1,
        },
      })
      .promise();
  }
}

export async function setup() {
  await createTableIfNotExists();

  const db = new DynamoDB.DocumentClient();
  await db
    .put({
      TableName: SECRETS_TABLE_NAME,
      Item: { key: secretKey, value: EMAIL_API_KEY },
    })
    .promise();
}

setup();
