# Greenjinn-test

A typescript Serverless project that provides some APIs executed on different lambda functions.

- GET averageTickers

  Returns the average of ticker values provided by some Bitcoin exchanges for the BTC -> USD.

- POST sendTickersViaEmail

  Send an email with the current average ticker value.

  - JSON Request body `{ "email": "me@domain.com"}`

## Requirements

- [Node](https://nodejs.org)
- [Serverless](https://www.serverless.com)
- [AWS account](https://aws.amazon.com)
- [Mailgun API key](https://www.mailgun.com/)

## Getting started

1. Install the Serverless framework

   ```bash
   npm install -g serverless
   ```

2. [Setting up AWS credentials](https://www.serverless.com/framework/docs/providers/aws/guide/credentials/)

3. Setting up mailgun service API key

   Sign up and create an API key on mailgun to send email messages.

   Run `setupSecrets.ts` to store your secret API key on AWS.

   ```bash
   EMAIL_API_KEY={YOUR_API_KEY} ts-node setupSecret.ts
   ```

   API key will be stored for simplicity on a dynamodb table and safely retrieved at runtime by sendTickersViaEmail lambda fn.

   A future solution will be to use [AWS Secrets Manager](https://aws.amazon.com/secrets-manager/) instead of dynamodb.

## Deploy

```bash
sls deploy
```

## GetAverageTickers API

This API makes requests to external services to get the current ticker values.

A retry logic was implemented to handle request failures. (Max 3 retries with a 1 sec delay)

Behaviour if some requests fail:

- [x] It should compute the average if at least one request succedeed.
- [ ] It should fail!

## Future improvements

- Some kind of logic to rate limit the requests to external bitcoin exchanger APIs

  (If limit reached, services could ban the ip address)

- Add a better looking email template.
