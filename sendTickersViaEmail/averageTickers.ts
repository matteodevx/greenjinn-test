import { Lambda } from "aws-sdk";

export async function getAverageTickers() {
  const stage = process.env.AWS_STAGE ?? "dev";
  if (stage == null) {
    throw new Error("AWS_STAGE env var undefined");
  }

  const lambda = new Lambda();

  const res = await lambda
    .invoke({
      InvocationType: "RequestResponse",
      FunctionName: `greenjinn-test-${stage}-averageTickers`,
    })
    .promise()
    .then((res) => JSON.parse(<string | undefined>res.Payload || "{}"));

  if (res.statusCode != 200) {
    return undefined;
  }

  try {
    const body = JSON.parse(res.body ?? "{}");
    return body.avg as number | undefined;
  } catch {
    return undefined;
  }
}
