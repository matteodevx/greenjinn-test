import { createTransport, SendMailOptions } from "nodemailer";
import * as mg from "nodemailer-mailgun-transport";

export interface SendmailParams {
  auth: {
    api_key: string;
    domain: string;
  };
  options: SendMailOptions;
}

export async function sendmail(params: SendmailParams) {
  const { auth, options } = params;
  const mailgun = createTransport(mg({ auth }));
  return mailgun.sendMail(options);
}
