import { APIGatewayProxyHandler } from "aws-lambda";
import { getAverageTickers } from "./averageTickers";
import { sendmail } from "./sendmail";
import { getEmailApiKey } from "../utils";

let apiKey: string | undefined;

export const sendTickersViaEmail: APIGatewayProxyHandler = async (
  event,
  _context
) => {
  const emailFrom = process.env.EMAIL_FROM;
  const domain = process.env.MAILER_DOMAIN;

  // If value not cached. Retrieve apikey
  if (apiKey == null) {
    apiKey = await getEmailApiKey();
  }

  // Safe check. This should not happen if everything is set.
  if (emailFrom == null || apiKey == null || domain == null) {
    return {
      statusCode: 500,
      body: "Internal Server Error",
    };
  }

  const body = JSON.parse(event.body ?? "{}");
  const email: string | undefined = body.email;

  if (email == null) {
    return {
      statusCode: 400,
      body: "Missing email",
    };
  }

  // Avg could be null if all requests to external services failed
  const avg = await getAverageTickers();

  if (avg == null) {
    return {
      statusCode: 500,
      body: "Internal Server Error. Retry later",
    };
  }

  const subject = "BTC-USD average ticker value";
  const text = `Hello,\n\nthe currently BTC-USD average ticker value is ${avg.toFixed(
    2
  )}`;

  try {
    await sendmail({
      auth: {
        api_key: apiKey,
        domain,
      },
      options: { from: emailFrom, to: email, subject, text },
    });

    return {
      statusCode: 200,
      body: "Ok",
    };
  } catch (err) {
    return {
      statusCode: 500,
      body: err.toString(),
    };
  }
};
