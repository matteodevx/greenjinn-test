import { APIGatewayProxyHandler } from "aws-lambda";
import {
  getBitstampTickerValue,
  getCoinbaseTickerValue,
  getBitfinexTickerValue,
} from "./tickerRequests";

export async function avgTickerValues() {
  const res = await Promise.all([
    getBitstampTickerValue(),
    getCoinbaseTickerValue(),
    getBitfinexTickerValue(),
  ]);

  // Compact values to remove failed responses
  const values = res.reduce(
    (prev, next) => (next != null ? [...prev, next] : prev),
    [] as number[]
  );

  if (values.length === 0) {
    return null;
  }

  return values.reduce((prev, next) => prev + next, 0) / values.length;
}

export const averageTickers: APIGatewayProxyHandler = async (
  _event,
  _context
) => {
  const avg = await avgTickerValues();

  if (avg == null) {
    return {
      statusCode: 500,
      body: "Internal Server Error",
    };
  }

  return {
    statusCode: 200,
    body: JSON.stringify({ avg }),
  };
};
