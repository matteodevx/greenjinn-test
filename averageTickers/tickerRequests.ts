import { default as axios } from "axios";
import { retry } from "../utils";

export async function getBitstampTickerValue() {
  const url = "https://www.bitstamp.net/api/v2/ticker/btcusd";
  const ax = axios.create({
    url,
  });
  const res = await retry({ ax });
  const value = Number(res?.data.last);
  return isNaN(value) ? null : value;
}

export async function getCoinbaseTickerValue() {
  const url = "https://api.coinbase.com/v2/exchange-rates?currency=BTC";
  const ax = axios.create({ url });
  const res = await retry({ ax });
  const value = Number(res?.data.data?.rates?.USD);
  return isNaN(value) ? null : value;
}

export async function getBitfinexTickerValue() {
  const url = "https://api-pub.bitfinex.com/v2/tickers?symbols=tBTCUSD";
  const ax = axios.create({ url });
  const res = await retry({ ax });
  const data = res?.data;
  const value = Number(data != null && data[0] != null ? data[0][7] : null);
  return isNaN(value) ? null : value;
}
