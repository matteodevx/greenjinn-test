import { Serverless } from "serverless/aws";

const serverlessConfiguration: Serverless = {
  service: {
    name: "greenjinn-test",
  },
  frameworkVersion: ">=1.72.0",
  custom: {
    webpack: {
      webpackConfig: "./webpack.config.js",
      includeModules: true,
    },
  },
  // Add the serverless-webpack plugin
  plugins: ["serverless-webpack"],
  provider: {
    name: "aws",
    runtime: "nodejs12.x",
    apiGateway: {
      minimumCompressionSize: 1024,
    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: "1",
      AWS_STAGE: "${opt:stage, self:provider.stage}",
    },
    iamRoleStatements: [
      {
        Effect: "Allow",
        Action: "dynamodb:GetItem",
        Resource: "arn:aws:dynamodb:*",
      },
      {
        Effect: "Allow",
        Action: "lambda:InvokeFunction",
        Resource: "arn:aws:lambda:*",
      },
    ],
  },
  functions: {
    averageTickers: {
      handler: "averageTickers/handler.averageTickers",
      events: [
        {
          http: {
            method: "get",
            path: "averageTickers",
          },
        },
      ],
    },
    sendTickersViaEmail: {
      handler: "sendTickersViaEmail/handler.sendTickersViaEmail",
      environment: {
        EMAIL_FROM: "info@test.com",
        MAILER_DOMAIN: "sandbox2b8da1a89ce643d99aef88c76259bcec.mailgun.org",
      },
      events: [
        {
          http: {
            method: "POST",
            path: "sendTickersViaEmail",
          },
        },
      ],
    },
  },
};

module.exports = serverlessConfiguration;
