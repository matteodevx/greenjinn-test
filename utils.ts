import { AxiosInstance } from "axios";
import { DynamoDB } from "aws-sdk";

export const SECRETS_TABLE_NAME = "secrets";
export const secretKey = "emailApiKey";

export async function delay(time: number) {
  return new Promise((res) => setTimeout(() => res(), time));
}

export async function retry(params: {
  ax: AxiosInstance;
  n?: number;
  waitFor?: number;
}) {
  const { ax } = params;
  let n = params.n ?? 3;
  const waitFor = params.waitFor ?? 1000;

  let shouldRetry = false;
  do {
    try {
      const res = await ax.request({});
      return res;
    } catch (err) {
      shouldRetry = --n > 0;
      await delay(waitFor);
    }
  } while (shouldRetry);
}

export async function getEmailApiKey() {
  const db = new DynamoDB.DocumentClient();
  const { Item } = await db
    .get({ TableName: SECRETS_TABLE_NAME, Key: { key: secretKey } })
    .promise();
  return Item ? Item.value : null;
}
