const { sendTickersViaEmail } = require("../sendTickersViaEmail/handler");

beforeAll(() => {
  process.env = Object.assign(process.env, {
    EMAIL_FROM: "info@test.com",
    MAILER_DOMAIN: "dummy_mailer",
  });
});

jest.mock("../utils");
const { getEmailApiKey } = require("../utils");
getEmailApiKey.mockImplementation(() => "dummy_api_key");

jest.mock("../sendTickersViaEmail/sendmail");
const { sendmail } = require("../sendTickersViaEmail/sendmail");
sendmail.mockImplementation(() => null);

jest.mock("../sendTickersViaEmail/averageTickers");
const { getAverageTickers } = require("../sendTickersViaEmail/averageTickers");

it("should send an email", async () => {
  getAverageTickers.mockImplementation(() => 10000);
  const body = JSON.stringify({ email: "test@test.com" });
  const event = { body };

  const res = await sendTickersViaEmail(event, {});

  expect(res.statusCode).toEqual(200);
});

it("should not send an email if no email given in payload event", async () => {
  getAverageTickers.mockImplementation(() => 10000);
  const body = JSON.stringify({});
  const event = { body };

  const res = await sendTickersViaEmail(event, {});

  expect(res.statusCode).toEqual(400);
});

it("should not send an email if averageTickers request fails", async () => {
  getAverageTickers.mockImplementation(() => null);
  const body = JSON.stringify({ email: "test@test.com" });
  const event = { body };

  const res = await sendTickersViaEmail(event, {});

  expect(res.statusCode).toEqual(500);
});
