import { avgTickerValues } from "../averageTickers/handler";

jest.mock("../averageTickers/tickerRequests");
const {
  getBitstampTickerValue,
  getCoinbaseTickerValue,
  getBitfinexTickerValue,
} = require("../averageTickers/tickerRequests");

it("should compute the avg of ticker values", async () => {
  const bitstampValue = 10000;
  const coinbaseValue = 10600;
  const bitfinexValue = 10300;

  getBitstampTickerValue.mockImplementation(() => bitstampValue);
  getCoinbaseTickerValue.mockImplementation(() => coinbaseValue);
  getBitfinexTickerValue.mockImplementation(() => bitfinexValue);

  const avg = await avgTickerValues();

  const expectedAvg = (bitstampValue + coinbaseValue + bitfinexValue) / 3;

  expect(avg).toEqual(expectedAvg);
});

it("should compute the avg of ticker values if at least one request succedeed", async () => {
  const bitstampValue = 10000;
  const coinbaseValue = null;
  const bitfinexValue = null;

  getBitstampTickerValue.mockImplementation(() => bitstampValue);
  getCoinbaseTickerValue.mockImplementation(() => coinbaseValue);
  getBitfinexTickerValue.mockImplementation(() => bitfinexValue);

  const avg = await avgTickerValues();

  const expectedAvg = bitstampValue;

  expect(avg).toEqual(expectedAvg);
});

it("should fail to compute the avg of ticker values if all API requests failed", async () => {
  const bitstampValue = null;
  const coinbaseValue = null;
  const bitfinexValue = null;

  getBitstampTickerValue.mockImplementation(() => bitstampValue);
  getCoinbaseTickerValue.mockImplementation(() => coinbaseValue);
  getBitfinexTickerValue.mockImplementation(() => bitfinexValue);

  const avg = await avgTickerValues();

  expect(avg).toEqual(null);
});
